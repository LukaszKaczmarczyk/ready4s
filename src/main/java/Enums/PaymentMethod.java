package Enums;

public enum PaymentMethod {

    PAYPAL,
    CASH_ON_DELIVERY,
    DIRECT_BANK_TRANSFER
}
