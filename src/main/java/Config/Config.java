package Config;

import java.io.InputStream;
import java.util.Properties;

public class Config {

    private Properties properties;

    public Config() {
        properties = getProperties();
    }

    private Properties getProperties() {
        Properties prop = new Properties();
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("config.properties");
            prop.load(inputStream);
        } catch (Exception e) {
            throw new RuntimeException("Cannot load properties file: " + e);
        }
        return prop;
    }

    public String getApplicationUrl() {
        return properties.getProperty("APPLICATION_URL");
    }

    public String getMailURL() {
        return properties.getProperty("MAIL_URL");
    }

    public String getFixedUserName() {
        return properties.getProperty("FIXED_USER_NAME");
    }

    public String getFixedPassword() {
        return properties.getProperty("FIXED_PASSWORD");
    }

    public String getFixedMailAddress() {
        return properties.getProperty("FIXED_MAIL_ADDRESS");
    }
}
