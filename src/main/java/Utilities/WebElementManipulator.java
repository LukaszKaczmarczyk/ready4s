package Utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class WebElementManipulator {

    protected WebDriver driver;
    protected final Wait<WebDriver> wait;
    protected JavascriptExecutor jse;

    public WebElementManipulator(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 20);
        jse = (JavascriptExecutor) driver;
    }

    public WebElement findElementByCssSelector(String cssSelector) {
        return driver.findElement(By.cssSelector(cssSelector));
    }

    public void click(WebElement element) {
        element.click();
    }

    public void setText(WebElement element, String text) {
        element.sendKeys(text);
    }

    public WebElement randomElement(List<WebElement> list) {
        int i = (int) (Math.random() * list.size());
        return list.get(i);
    }

    public void waitUntilElementIsVisible(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitUntilElementIsClickable(WebElement element) {
        wait.until(p -> isElementIsClickable(element));
    }

    public void waitUntilElementIsClickableAndClick(WebElement element) {
        wait.until(p -> isElementIsClickable(element));
        click(element);
    }

    public static boolean isElementIsClickable(WebElement element) {
        return element.isDisplayed() && element.isEnabled();
    }

    public void waitUntilPageIsLoaded() {
        wait.until(p -> jse.executeScript("return document.readyState").toString().equals("complete"));
    }

    public void clickByJSE(WebElement element) {
        jse.executeScript("arguments[0].click();", element);
    }

    public void scrollUp() {
        jse.executeScript("scrollBy(0,-250);");
    }

    public void scrollDownWith250px() {
        jse.executeScript("window.scrollTo(document.body.scrollHeight,0)");
    }

    public void scrollDown() {
        jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    public void scrollTillElement(WebElement element) {
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].scrollIntoView();", element);
    }

    public void waitUntilFrameWillBeAvailableAndSwitchToIt(By by) {
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(by));
    }

    public void implicitWait(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public void moveToElement(WebElement element) {
        Actions actionsM = new Actions(driver);
        actionsM.moveToElement(element).build().perform();
    }

    public void switchToIframe(String iframeName) {
        driver.switchTo().frame(iframeName);
    }

    public void getWindowHandles(WebDriver driver, int i) {
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(i));
    }
}
