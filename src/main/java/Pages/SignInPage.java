package Pages;

import Config.Config;
import Utilities.Utils;
import Utilities.WebElementManipulator;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignInPage extends WebElementManipulator {

    public SignInPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "user_login")
    private WebElement userLogin;

    @FindBy(id = "user_email")
    private WebElement userEmail;

    @FindBy(id = "wp-submit")
    private WebElement submitRegisterLogInButton;

    @FindBy(css = "#login > p.message")
    private WebElement succesfullRegisterMessage;

    @FindBy(id = "user_pass")
    private WebElement userPasswordField;

    @FindBy(xpath = "//*[contains(text(),'podano nieprawidłowe hasło')")
    private WebElement errorMessage;

    private MailPage mailPage;
    private static String defaultUserName = Utils.generateRandomString(8);
    private static String defaultPassword = Utils.generateRandomString(6);
    private String mailClient = "@yopmail.com";

    public static String getDefaultUserName() {
        return defaultUserName;
    }

    public SignInPage setCredentials() {
        waitUntilPageIsLoaded();
        userLogin.isEnabled();
        waitUntilElementIsClickableAndClick(userLogin);
        setText(userLogin, getDefaultUserName());
        setText(userEmail, getDefaultUserName() + mailClient);
        return this;
    }

    public SignInPage submitRegisterButton() {
        waitUntilElementIsClickableAndClick(submitRegisterLogInButton);
        validateCorrectRegister();
        return this;
    }

    public MyAccountPage logInWithCredentials() {
        waitUntilElementIsVisible(userLogin);
        waitUntilElementIsClickable(userLogin);
        setText(userLogin, new Config().getFixedUserName());
        setText(userPasswordField, new Config().getFixedPassword());
        click(submitRegisterLogInButton);
        return new MyAccountPage(driver);
    }

    public MyAccountPage logInAfterRegistrationAndSetPassword() {
        getWindowHandles(driver, 1);
        validateCorrectActivateLink();
        waitUntilElementIsClickable(userLogin);
        setText(userLogin, getDefaultUserName() + mailClient);
        setText(userPasswordField, defaultPassword);
        click(submitRegisterLogInButton);
        invalidMailPassword();
        return new MyAccountPage(driver);
    }

    private void validateCorrectRegister() {
        Assertions.assertEquals("Rejestracja została ukończona. Proszę sprawdzić emaila.", succesfullRegisterMessage.getText());
    }

    private void invalidMailPassword() {
        Assertions.assertTrue(errorMessage.getText().contains("podano nieprawidłowe hasło"));
    }

    private void validateCorrectActivateLink() {
        Assertions.assertTrue(driver.getCurrentUrl().contains("ready4s.it/automat/wp-login.php"), "Wrongly current URL");
    }
}
