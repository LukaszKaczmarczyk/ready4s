package Pages;

import Utilities.WebElementManipulator;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CheckoutPage extends WebElementManipulator {

    private String addressTextCssSelector = "Address";
    private String cashOnDeliveryPaymentMethodXPath = "//*[contains(text(),'Cash on delivery')]";

    public CheckoutPage(WebDriver driver) {
        super(driver);
    }

    private WebElement getAddressText() {
        return driver.findElement(By.cssSelector(addressTextCssSelector));
    }

    private WebElement getPaymentMethod() {
        return driver.findElement(By.xpath(cashOnDeliveryPaymentMethodXPath));
    }

    public void validateDataFromCheckoutPage() {
        validateCahOnDeliveryPaymentMethod();
        validateGetAddressText();
    }

    public void validateCahOnDeliveryPaymentMethod() {
        Assertions.assertEquals("Cash om delivery", getPaymentMethod().getText(), "Cash on delivery payment method has not been found");
    }

    public void validateGetAddressText() {
        getAddressText().isDisplayed();
    }
}
