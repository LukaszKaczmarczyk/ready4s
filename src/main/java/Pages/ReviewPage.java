package Pages;

import Utilities.Utils;
import Utilities.WebElementManipulator;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class ReviewPage extends WebElementManipulator {

    public ReviewPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "div[class=comment-text]")
    private List<WebElement> reviewComments;

    private String commentReviewTextArea = "return document.querySelector('#comment')";
    private String reviewTabQueryJseScript = "return document.querySelector('.tabs .reviews_tab')";
    private String mark5stars = "return document.querySelector('#commentform > div > p > span > a.star-5')";
    private String publishButton = "return document.querySelector('#submit')";

    private String randomReviewComment = Utils.generateRandomString(5);


    public WebElement getReviewsTab() {
        WebElement tab = (WebElement) jse.executeScript(reviewTabQueryJseScript);
        return tab;
    }

    public WebElement getMark5Stars() {
        WebElement star = (WebElement) jse.executeScript(mark5stars);
        return star;
    }

    public WebElement getPublishButton() {
        WebElement submitBtn = (WebElement) jse.executeScript(publishButton);
        return submitBtn;
    }

    public WebElement getCommentReviewtextArea() {
        WebElement comment = (WebElement) jse.executeScript(commentReviewTextArea);
        return comment;
    }

    public ReviewPage clickReviewsTab() {
        waitUntilElementIsClickable(getReviewsTab());
        click(getReviewsTab());
        return this;
    }

    public ReviewPage markReviewStars() {

        scrollUp();
        wait.until(ExpectedConditions.visibilityOf(getMark5Stars()));
        click(getMark5Stars());
        return this;
    }

    public ReviewPage addCommentReview() {
        scrollDown();
        waitUntilElementIsClickable(getCommentReviewtextArea());
        setText(getCommentReviewtextArea(), randomReviewComment);
        return this;
    }

    public ReviewPage clickPublishButton() {
        scrollDown();
        waitUntilElementIsClickable(getPublishButton());
        click(getPublishButton());
        validateReviewCommentIsPresented();
        return this;
    }

    private void validateReviewCommentIsPresented() {
        for (int i = 0; i < reviewComments.size(); i++) {
            Assertions.assertTrue(reviewComments.get(i).getText().contains(randomReviewComment), "Review comment can not be find");
        }
    }
}

