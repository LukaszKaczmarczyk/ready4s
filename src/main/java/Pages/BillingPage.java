package Pages;

import Config.Config;
import Enums.PaymentMethod;
import Utilities.Utils;
import Utilities.WebElementManipulator;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class BillingPage extends WebElementManipulator {

    public BillingPage(WebDriver driver) {
        super(driver);
    }

    private String placeHolderButtonQueryJseScript = "return document.querySelector('#place_order')";
    private String cashOnDeliveryRadioButtonQueryJseScript = "return document.querySelector('#payment_method_cod')";
    private String orderReceivedMessage = ".entry-title";
    private String billingFirstNameCssSelector = "#billing_first_name.input-text";
    private String billingLastNameCssSelector = "#billing_last_name.input-text";
    private String billingAddress1CssSelector = "#billing_address_1.input-text";
    private String billingPostcodeCssSelector = "#billing_postcode.input-text";
    private String billingCityCssSelector = "#billing_city.input-text";
    private String billingPhoneCssSelector = "#billing_phone.input-text";
    private String billingEmailCssSelector = "#billing_email.input-text";

    private WebElement getPlaceHolderButton() {
        return (WebElement) jse.executeScript(placeHolderButtonQueryJseScript);
    }

    private WebElement getCashOnDeliveryRadioButton() {
        return (WebElement) jse.executeScript(cashOnDeliveryRadioButtonQueryJseScript);
    }

    private WebElement getOrderReceivedMessage() {
        return driver.findElement(By.cssSelector(orderReceivedMessage));
    }

    public BillingPage clearAllBillingDetails() {
        findElementByCssSelector(billingFirstNameCssSelector).clear();
        findElementByCssSelector(billingLastNameCssSelector).clear();
        findElementByCssSelector(billingAddress1CssSelector).clear();
        findElementByCssSelector(billingPostcodeCssSelector).clear();
        findElementByCssSelector(billingCityCssSelector).clear();
        findElementByCssSelector(billingPhoneCssSelector).clear();
        findElementByCssSelector(billingEmailCssSelector).clear();
        return this;
    }

    public BillingPage setBillingDetails() {
        findElementByCssSelector(billingFirstNameCssSelector).sendKeys(Utils.generateRandomString(5));
        findElementByCssSelector(billingLastNameCssSelector).sendKeys(Utils.generateRandomString(8));
        findElementByCssSelector(billingAddress1CssSelector).sendKeys(Utils.generateRandomInt(1, 100) + " " + Utils.generateRandomString(8));
        findElementByCssSelector(billingPostcodeCssSelector).sendKeys(Utils.generateRandomInt(10, 99) + "-" + Utils.generateRandomInt(100, 999));
        findElementByCssSelector(billingCityCssSelector).sendKeys(Utils.generateRandomString(8));
        findElementByCssSelector(billingPhoneCssSelector).sendKeys(Utils.generateRandomInt(100, 999) + "-" + Utils.generateRandomInt(100, 999) + "-" + Utils.generateRandomInt(100, 999));
        findElementByCssSelector(billingEmailCssSelector).sendKeys(new Config().getFixedMailAddress());
        return this;
    }


    public BillingPage selectPaymentMethod(PaymentMethod paymentMethod) {
        switch (paymentMethod) {
            case CASH_ON_DELIVERY:
                scrollDown();
                waitUntilPageIsLoaded();
                scrollTillElement(getCashOnDeliveryRadioButton());
                waitUntilElementIsClickable(getCashOnDeliveryRadioButton());
                try {
                    click(getCashOnDeliveryRadioButton());
                } catch (Exception e) {
                    click(getCashOnDeliveryRadioButton());
                }
                break;
            case PAYPAL:
                //not implemented yet
                break;
            case DIRECT_BANK_TRANSFER:
                //not implemented yet
                break;
            default:
                System.out.println("The option is not present in menu. Please try again.");
        }
        return this;
    }

    public CheckoutPage clickPlaceHolderAndWaitForPageIsLoaded() {
        click((getPlaceHolderButton()));
        wait.until(ExpectedConditions.invisibilityOfElementWithText(By.cssSelector(orderReceivedMessage), "Checkout"));
        validateOrderIsReceived();
        return new CheckoutPage(driver);
    }

    private void validateOrderIsReceived() {
        Assertions.assertEquals("Order received", getOrderReceivedMessage().getText(), "Wrongly order received message");
    }
}
