package Pages;

import Utilities.WebElementManipulator;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends WebElementManipulator {

    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(linkText = "Zarejestruj się")
    private WebElement signIn;

    @FindBy(linkText = "Zaloguj się")
    private WebElement logIn;

    @FindBy(css = "#login > p.message.register")
    private WebElement registerMessage;

    public SignInPage clickSignIn() {
        waitUntilPageIsLoaded();
        waitUntilElementIsClickable(signIn);
        click(signIn);
        validateRegisterMessage();
        return new SignInPage(driver);
    }

    public SignInPage clickLogIn() {
        waitUntilPageIsLoaded();
        waitUntilElementIsClickable(signIn);
        click(logIn);
        return new SignInPage(driver);
    }

    public void validateRegisterMessage() {
        Assertions.assertEquals("Zarejestruj się na tej witrynie", registerMessage.getText(), "Wrongly register message");
    }
}

