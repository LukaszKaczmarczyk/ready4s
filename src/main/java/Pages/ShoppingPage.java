package Pages;

import Utilities.WebElementManipulator;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ShoppingPage extends WebElementManipulator {

    public ShoppingPage(WebDriver driver) {
        super(driver);
    }

    private String addToCartButtonQueryJseScript = "return document.querySelector('.cart .button')";
    private String viewCartButtonQueryJseScript = "return document.querySelector('#main > div.woocommerce-notices-wrapper > div > a')";
    private String proceedToCheckoutButtonQueryJseScript = "return document.querySelector('#post-29 > div > div > div.cart-collaterals > div > div > a')";
    private String quantityFieldCssSelector = "#quantity_5c8f27aa3df02.input-text.qty.text";
    private String addToCartMessageCssSelector = "div.woocommerce-message";

    private WebElement getAddToCartButton() {
        return (WebElement) jse.executeScript(addToCartButtonQueryJseScript);
    }

    private WebElement getViewCartButton() {
        return (WebElement) jse.executeScript(viewCartButtonQueryJseScript);
    }

    private WebElement getProceedToCheckoutButton() {
        return (WebElement) jse.executeScript(proceedToCheckoutButtonQueryJseScript);
    }

    public ShoppingPage clickAddToCart() {
        click(getAddToCartButton());
        validateAddProductToCart();
        return this;
    }

    public ShoppingPage clickViewCart() {
        click(getViewCartButton());
        return this;
    }

    public ShoppingPage changeQuantity(String quantity) {
        findElementByCssSelector(quantityFieldCssSelector).clear();
        setText(findElementByCssSelector(quantityFieldCssSelector), quantity);
        return this;
    }

    public ShoppingPage proceedToCheckout() {
        scrollDown();
        click(getProceedToCheckoutButton());
        return this;
    }

    public void validateAddProductToCart() {
        Assertions.assertTrue(findElementByCssSelector(addToCartMessageCssSelector).getText().contains("has been added to your cart"));
    }
}
