package Pages;

import Config.Config;
import Utilities.WebElementManipulator;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class MailPage extends WebElementManipulator {

    public MailPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "login")
    private WebElement loginMailField;
    @FindBy(css = "input[class=sbut]")
    private WebElement checkMailButton;
    @FindBy(css = "span[class=lms]")
    private List<WebElement> inboxMailListHeaders;
    @FindBy(css = "a[href*=ready4s]")
    private WebElement activeLink;
    @FindBy(xpath = "//*[text()[contains(.,'Nazwa')]]")
    private WebElement nameUserFromMail;

    public MailPage createMail() {
        driver.get(new Config().getMailURL());
        waitUntilPageIsLoaded();
        setText(loginMailField, SignInPage.getDefaultUserName());
        click(checkMailButton);
        return this;
    }

    public MailPage selectMail(String mailName) {
        switchToIframe("ifinbox");
        for (int i = 0; i < inboxMailListHeaders.size(); i++) {
            if (inboxMailListHeaders.get(i).getText().contains(mailName))
                click(inboxMailListHeaders.get(i));
        }
        return this;
    }

    public SignInPage activateLinkAndLogIn() {
        driver.switchTo().defaultContent();
        switchToIframe("ifmail");
        validateEmailAdress();
        click(activeLink);
        return new SignInPage(driver);
    }

    private String getEmailAddress() {
        return nameUserFromMail.getText();
    }

    private void validateEmailAdress() {
        Assertions.assertTrue(getEmailAddress().contains(SignInPage.getDefaultUserName()), "Incorrect mail address");
    }
}
