package Pages;

import Utilities.Utils;
import Utilities.WebElementManipulator;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class MyAccountPage extends WebElementManipulator {

    @FindBy(css = "#post-31 > header > h1")
    private WebElement myAccountphrase;
    @FindBy(css = "#search-3 > form > label > input")
    private WebElement searchField;
    @FindBy(css = "#search-3 > form > button")
    WebElement searchLoopIcon;
    @FindBy(css = "span[class=comments-link]>a")
    private List<WebElement> addCommentList;
    @FindBy(id = "comment")
    private WebElement commentField;
    @FindBy(id = "submit")
    private WebElement submitCommentButton;
    @FindBy(css = "a[href*=product")
    private WebElement linkProduct;
    @FindBy(css = "div[class=comment-content]")
    private List<WebElement> comments;

    private static final String TITLE_PAGE = "My account";
    private String randomComment = Utils.generateRandomString(5);
    private String textKeywoard;

    public MyAccountPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public MyAccountPage searchKeyword(String text) {
        textKeywoard = text;
        setText(searchField, textKeywoard);
        click(searchLoopIcon);
        return this;
    }

    public ShoppingPage selectRightProduct() {
        validateProductName();
        click(linkProduct);
        return new ShoppingPage(driver);
    }

    public MyAccountPage addCommentForArticle(int index) {
        waitUntilElementIsClickableAndClick(addCommentList.get(index));
        setText(commentField, randomComment);
        scrollDown();
        waitUntilElementIsClickable(submitCommentButton);
        click(submitCommentButton);
        validateIsCommentIsPresented();
        return this;
    }

    public void validateCorrectLoggedIn() {
        Assertions.assertEquals(TITLE_PAGE, myAccountphrase.getText(), "Incorrect loggedIn");
    }

    private void validateIsCommentIsPresented() {
        for (int i = 0; i < comments.size(); i++) {
            if (comments.get(i).getText().contains(randomComment)) {
                Assertions.assertTrue(comments.get(i).getText().contains(randomComment), "Comment can not be find");
            }
        }
    }

    public void validateProductName() {
        String[] splitExpectedValue = linkProduct.getText().split(" ");
        Assertions.assertTrue(textKeywoard.contains(splitExpectedValue[0]), "Incorrect login product name");
        Assertions.assertTrue(textKeywoard.contains(splitExpectedValue[1]), "Incorrect login product name");
    }
}
