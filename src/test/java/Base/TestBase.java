package Base;

import Config.Config;
import Utilities.Utils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.nio.file.Paths;

public class TestBase {

    protected WebDriver driver;

    public void takesScreenshot() throws Exception {
        TakesScreenshot takesScreenshot = ((TakesScreenshot) driver);
        String fileDestination = "resources/screens/ready4s" + Utils.getDateAndTime() + Utils.generateRandomString(5) + ".png";
        File sourceFile = takesScreenshot.getScreenshotAs(OutputType.FILE);
        org.apache.commons.io.FileUtils.copyFile(sourceFile, new File(fileDestination));
    }

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", Paths.get(".").toAbsolutePath().normalize().toString() + "\\src\\main\\resources\\Drivers\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
        driver.get(new Config().getApplicationUrl());
    }

    @After
    public void tearDown() throws Exception {
        takesScreenshot();
        driver.quit();
    }
}
