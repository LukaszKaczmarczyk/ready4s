package Tests;

import Base.TestBase;
import Pages.HomePage;
import Pages.MyAccountPage;
import Pages.ReviewPage;
import Pages.SignInPage;
import org.junit.Test;

public class AddReviewTest extends TestBase {

    @Test
    public void addReviewTest() {

        HomePage homePage = new HomePage(driver);
        SignInPage signInPage = new SignInPage(driver);
        MyAccountPage myAccountPage = new MyAccountPage(driver);
        ReviewPage reviewPage = new ReviewPage(driver);
        homePage.clickLogIn();
        signInPage.logInWithCredentials();
        myAccountPage.validateCorrectLoggedIn();
        myAccountPage.searchKeyword("Test product 2").
                selectRightProduct();
        reviewPage.clickReviewsTab().
                markReviewStars().
                addCommentReview().
                clickPublishButton();
    }
}
