package Tests;

import Base.TestBase;
import Pages.HomePage;
import Pages.MailPage;
import Pages.MyAccountPage;
import Pages.SignInPage;
import org.junit.Test;

public class SignUpTest extends TestBase {

    @Test
    public void signUpTest() {

        HomePage homePage = new HomePage(driver);
        SignInPage signInPage = new SignInPage(driver);
        MailPage mailPage = new MailPage(driver);
        MyAccountPage myAccountPage = new MyAccountPage(driver);
        homePage.clickSignIn();
        signInPage.setCredentials()
                .submitRegisterButton();
        mailPage.createMail().
                selectMail("Ready4S").
                activateLinkAndLogIn()
                .logInAfterRegistrationAndSetPassword();
        myAccountPage.validateCorrectLoggedIn();
    }
}
