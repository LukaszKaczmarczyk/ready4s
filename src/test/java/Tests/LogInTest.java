package Tests;

import Base.TestBase;
import Pages.HomePage;
import Pages.MyAccountPage;
import Pages.SignInPage;
import org.junit.Test;

public class LogInTest extends TestBase {

    @Test
    public void logInTest() {

        HomePage homePage = new HomePage(driver);
        SignInPage signInPage = new SignInPage(driver);
        MyAccountPage myAccountPage = new MyAccountPage(driver);
        homePage.clickLogIn();
        signInPage.logInWithCredentials();
        myAccountPage.validateCorrectLoggedIn();
    }
}
