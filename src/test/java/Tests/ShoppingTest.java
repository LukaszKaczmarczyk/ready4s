package Tests;

import Base.TestBase;
import Enums.PaymentMethod;
import Pages.*;
import org.junit.Test;

public class ShoppingTest extends TestBase {

    @Test
    public void shoppingTest() {

        HomePage homePage = new HomePage(driver);
        SignInPage signInPage = new SignInPage(driver);
        MyAccountPage myAccountPage = new MyAccountPage(driver);
        ShoppingPage shoppingPage = new ShoppingPage(driver);
        BillingPage billingPage = new BillingPage(driver);
        CheckoutPage checkoutPage = new CheckoutPage(driver);
        homePage.clickLogIn();
        signInPage.logInWithCredentials();
        myAccountPage.validateCorrectLoggedIn();
        myAccountPage.searchKeyword("Test product 2").
                selectRightProduct();
        shoppingPage.clickAddToCart().
                clickViewCart().
                proceedToCheckout();
        billingPage.clearAllBillingDetails().
                setBillingDetails()
                .selectPaymentMethod(PaymentMethod.CASH_ON_DELIVERY)
                .clickPlaceHolderAndWaitForPageIsLoaded();
        checkoutPage.validateDataFromCheckoutPage();


    }
}
